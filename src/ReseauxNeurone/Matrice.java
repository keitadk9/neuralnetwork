package ReseauxNeurone;

import java.io.Serializable;
import java.util.Random;

public class Matrice implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7824233145337855611L;
	private double tableau[][];
	
	public Matrice(int ligne,int colonne) {
		tableau = new double[ligne][colonne];
	}
	
	public Matrice(Matrice m ) {
		this(m.getLigne(),m.getColonne());
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				tableau[i][j] = m.tableau[i][j];
			}
		}
		
	}
	
	public Matrice (Matrice m1,Matrice m2) {
		tableau = new double[m1.getLigne()][m1.getColonne()];
		Random r = new Random();
		int ligne = r.nextInt(m1.getLigne());
		int colonne = r.nextInt(m1.getColonne());
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				
				
				if((i  < ligne && j < colonne)) {
					tableau[i][j] = m1.tableau[i][j];
				}else {
					tableau[i][j] = m2.tableau[i][j];
				}
				
				
			}
		}
	}
	
	public Matrice(double[][] tab) {
		this(tab.length,tab[0].length);
		
		for (int i = 0; i < tab.length; i++) {
			for (int j = 0; j < tab.length; j++) {
				tableau[i][j] = tab[i][j];
			}
		}
	}
	
	public Matrice(double[] vecteur) {
		this(vecteur.length,1);
		for (int i = 0; i < tableau.length; i++) {
			tableau[i][0] = vecteur[i];
		}
		
		
		
	}
	
	public Matrice addBias() {
		
		Matrice bias = new Matrice(getLigne()+1,getColonne());
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				bias.tableau[i][j]  = tableau[i][j];
			}
		}
		
		bias.tableau[getLigne()][0] = 1;
		return bias;
		
		
		
	}
	
	
	public Matrice matmul(Matrice b) {
		return matmul(this,b);
	}
	
	
	
	public void mult(double val) {
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				tableau[i][j] *= val;
			}
		}
	}
	
	public void add(double val) {
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				tableau[i][j] += val;
			}
		}
	}
	
	public void add(Matrice m) {
		
		if(m.getColonne() != getColonne() || m.getLigne() != getLigne()) {
			System.err.println("Les matrice n'ont pas le m�me format pour �tre additionner");
			return;
		}
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				tableau[i][j] += m.tableau[i][j];
			}
		}	
	}
	
	public void sub(Matrice m) {
		if(m.getColonne() != getColonne() || m.getLigne() != getLigne()) {
			System.err.println("Les matrice n'ont pas le m�me format pour �tre soustrait");
			return;
		}
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				tableau[i][j] -= m.tableau[i][j];
			}
		}
	}
	
	
	public static Matrice matAdd(Matrice a,Matrice b) {
		if(a.getColonne() != b.getColonne() || a.getLigne() != b.getLigne()) {
			System.err.println("Les matrice non pas le m�me format pour �tre additionner");
			return null;
		}
		
		Matrice result = new Matrice(a.getLigne(),a.getColonne());
		for (int i = 0; i < a.tableau.length; i++) {
			for (int j = 0; j < a.tableau[0].length; j++) {
				result.tableau[i][j] = a.tableau[i][j] + b.tableau[i][j];
			}
		}
		
		return result;
		
	}
	
	public static Matrice matSub(Matrice a,Matrice b) {
		if(a.getColonne() != b.getColonne() || a.getLigne() != b.getLigne()) {
			System.err.println("Les matrice non pas le m�me format pour �tre soustrait");
			return null;
		}
		
		Matrice result = new Matrice(a.getLigne(),a.getColonne());
		for (int i = 0; i < a.tableau.length; i++) {
			for (int j = 0; j < a.tableau[0].length; j++) {
				result.tableau[i][j] = a.tableau[i][j] - b.tableau[i][j];
			}
		}
		
		return result;
		
	}
	
	public static Matrice matmul(Matrice a  , Matrice b){ 
		   
		   int ligneA = a.tableau.length;
		   int colsA = a.tableau[0].length;
		   
		   int ligneB = b.tableau.length;
		   int colsB = b.tableau[0].length;
		   
		   
		   if(colsA != ligneB){
		     System.err.println("Les colonnes de A doivent matcher avec les lignes de B");
		     return null;
		   
		   }
		   
		   double val = 0;
		   
		   Matrice result = new Matrice(ligneA, colsB);
		   
		       
		   for(int i = 0 ; i< ligneA ;i++){
		     for(int j = 0 ; j < colsB; j++){
		       val = 0;
		       for(int p = 0 ; p < colsA; p++){
		          val+= a.tableau[i][p] * b.tableau[p][j];  
		       }
		       result.tableau[i][j] = val;  

		     }

		   }
		   return result;
	}
	
	public static Matrice hadamardMult(Matrice a  , Matrice b) {
		
		Matrice result = a.clone();
		
		for (int i = 0; i < result.tableau.length; i++) {
			for (int j = 0; j < result.tableau[0].length; j++) {
				result.tableau[i][j] *=  b.tableau[i][j];
			}
		}
		
		return result;
		
	}
	
	public static Matrice Dsigmoide(Matrice a ) {
		
		Matrice result = a.clone();
		for (int i = 0; i < result.tableau.length; i++) {
			for (int j = 0; j < result.tableau[0].length; j++) {	
				result.tableau[i][j] = a.tableau[i][j] * (1 - a.tableau[i][j]);
			}
		}
		return result;
	
	}
		
		
		
	
	
	public void activate() {
		
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				double temp  = tableau[i][j];
				tableau[i][j] = sigmoid(tableau[i][j]);
				if(Double.isNaN(tableau[i][j])) {
					System.out.println(i+","+j);
					System.out.println(temp);
				}
			}
		}
		
	}
	
	public static double sigmoid(double x) {
		return (double) ((double) 1 / (1+Math.exp(x*-1) ));
	}
	
	public void randomize() {
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {

				
				double val = (double) Math.random();
				if(Math.random() < 0.5)
					val*= -1;
				
				tableau[i][j] = val ;
			}
		}
	}
	
	public void randomize(double rate) {
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				if(Math.random() > rate)
					continue;
				
				double val = (double) (new Random()).nextGaussian()/5;
				if(Math.random() < 0.5)
					val*= -1;
				
				tableau[i][j] += val ;
				if(tableau[i][j] > 1)
					tableau[i][j] = 1;
				else if(tableau[i][j] < -1)
					tableau[i][j] = -1;
			}
		}
		
	}
	
	public Matrice transpose() {
		Matrice m = new Matrice(getColonne(),getLigne());
		
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				m.tableau[j][i] = tableau[i][j];
			}
		}
		return m;	
	}
	
	public int getLigne() {
		return tableau.length;
	}
	public int getColonne() {
		return tableau[0].length;
	}
	
	public double[][] getTab(){
		double[][] result = new double[getLigne()][getColonne()];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[0].length; j++) {
				result[i][j] = tableau[i][j];
			}
		}
		return result;
	}
	
	
	
	public Matrice clone() {
		return new Matrice(this);
	}
	public double get(int i , int j) {
		return tableau[i][j];
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		
		for (int i = 0; i < tableau.length; i++) {
			for (int j = 0; j < tableau[0].length; j++) {
				s.append(tableau[i][j]+" ");
			}
			s.append("\n");
		}
		return s.toString();
	}
	
	
}
