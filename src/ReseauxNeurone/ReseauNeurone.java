package ReseauxNeurone;

import java.io.Serializable;

public class ReseauNeurone implements Serializable {
	

	
	private static final long serialVersionUID = -2644008868782242327L;
	private Matrice[] r�seaux;
	private Matrice[] biases;
	private Matrice[] activationValues;
	private double learningRate;
	
	public ReseauNeurone(int nombreInput,int[] nombreHidden,int nombreOutput,float tauxApprentissage) {

		//colonne = nombre de neuronne � la couche pr�c�dente
		//ligne = nombre de neurone � la couche actuelle
		
		r�seaux = new Matrice[1+nombreHidden.length];
		biases = new Matrice[1+nombreHidden.length];
		activationValues = new Matrice[1+nombreHidden.length];
		learningRate = tauxApprentissage;
		
		
		if(nombreHidden.length > 0) {
			r�seaux[0] = new Matrice(nombreHidden[0],nombreInput);
			biases[0] = new Matrice(nombreHidden[0],1);
			
			r�seaux[r�seaux.length-1] = new Matrice(nombreOutput,nombreHidden[nombreHidden.length-1]);
			biases[r�seaux.length-1] = new Matrice(nombreOutput,1);
		}
		else {
			r�seaux[0] = new Matrice(nombreOutput,nombreInput+1);
			biases[0] = new Matrice(nombreOutput,1);
		}
		
		if(nombreHidden.length >= 2 ) {		
			for(int i = 1 ; i < r�seaux.length-1 ;++i) {
				int lignes;
				int colonnes;
				
				lignes = nombreHidden[i];
				colonnes = nombreHidden[i-1];
					
				r�seaux[i] = new Matrice(lignes, colonnes);	
				biases[i] = new Matrice(lignes,1);
			}
		}
		
		for(Matrice m : r�seaux) {
			m.randomize();
		}
		for(Matrice bias : biases)
			bias.randomize();
	}
	public ReseauNeurone(int nombreInput,int nombreOutput,float tauxApprentissage) {
		this(nombreInput,new int[] {} , nombreOutput,tauxApprentissage);

	}
	public ReseauNeurone(ReseauNeurone r1,ReseauNeurone r2) {
		r�seaux = new Matrice[r1.r�seaux.length];
		biases = new Matrice[r1.biases.length];
		
		for (int i = 0; i < r1.r�seaux.length; i++) {
			r�seaux[i] = new Matrice(r1.r�seaux[i],r2.r�seaux[i]);
			biases[i] = new Matrice(r1.biases[i],r2.biases[i]);
		}
	}
	
	public ReseauNeurone(ReseauNeurone parent) {
		
		r�seaux = new Matrice[parent.r�seaux.length];
		biases = new Matrice[parent.r�seaux.length];
		
		for (int i = 0; i < r�seaux.length; i++) {
			r�seaux[i] = parent.r�seaux[i].clone();
			biases[i] = parent.biases[i].clone();
		}
	}
	
	
	public Matrice output(Matrice inputs) {
		
		
		Matrice result = inputs.clone();
		
		for(int i = 0 ; i <r�seaux.length ;++i) {
			result = r�seaux[i].matmul(result);
			result.add(biases[i]);
			result.activate();
			activationValues[i] = result.clone();
		}

		//System.out.println("========================================");	
		return result;
	}
	
	public void train(Matrice inputs,Matrice targets) {
		
		Matrice outputs = output(inputs);
		Matrice error = Matrice.matSub(targets,outputs);
		
		for(int i = r�seaux.length-1 ; i >= 0 ; --i) {
			
			error.mult(learningRate);
			Matrice gradient = Matrice.Dsigmoide(activationValues[i]);

			gradient = Matrice.hadamardMult(error,gradient);
		
			biases[i].add(gradient);
			
			if(i > 0 ) {
				gradient =  Matrice.matmul(gradient,activationValues[i-1].transpose());
			}
			else {
				gradient = Matrice.matmul(gradient,inputs.transpose());
			}
			r�seaux[i].add(gradient);
			// calculate next error
			error = r�seaux[i].transpose().matmul(error);
			
		}
		
	}
	
	public void mutate(float rate) {

		
		for (int i = 0; i < r�seaux.length; i++) {
			r�seaux[i].randomize(rate);
			biases[i].randomize(rate);
		}	
		
	}
	
	public static ReseauNeurone crossOver(ReseauNeurone r1,ReseauNeurone r2) {
		return new ReseauNeurone(r1, r2);
	}
	
	public ReseauNeurone clone() {
		return new ReseauNeurone(this);
	}
	
	
	
	public String toString() {
		String s = "";
		
		for (Matrice matrice : r�seaux) {
			s+= matrice.toString()+ "\n";
		}
		
		return s;
	}
	
	
	
	
}
