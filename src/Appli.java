import processing.core.PApplet;
import ReseauxNeurone.*;
public class Appli extends PApplet {
	
	
	private ReseauNeurone r�seau;
	Matrice[][] dataSet;
	int nbTrain = 10000;
	int session = 0;

	public static void main(String[] args) {
		PApplet.main("Appli");
	}
	
	public void settings() {
		size(600,600);
	}
	
	public void setup() {
		r�seau = new ReseauNeurone(2,new int[] {2},1,0.01f);
		dataSet = new Matrice[4][2];
		dataSet[0] = new Matrice[] {new Matrice(new double[] {0,0}),new Matrice(new double[] {0})};
		dataSet[1] = new Matrice[] {new Matrice(new double[] {0,1}),new Matrice(new double[] {1})};
		dataSet[2] = new Matrice[] {new Matrice(new double[] {1,0}),new Matrice(new double[] {1})};
		dataSet[3] = new Matrice[] {new Matrice(new double[] {1,1}),new Matrice(new double[] {0})};
		
		
		
		
		

		

		
		
		
	}
	
	public void draw() {
		background(50);
		frameRate(30);
		
		//	troke();
		
		
		for(int i = 0 ; i < nbTrain ; ++i) {
			int index = (int) random(4);
			r�seau.train(dataSet[index][0],dataSet[index][1]);
			session++;
		}
		
		
		for(int x = 0 ; x < width ; x+=10) {
			for(int y = 0 ; y < height ; y+=10) {
				Matrice guess = r�seau.output(new Matrice(new double[] {(double)x/600,(double)y/600}));
				
				fill((int) (guess.get(0,0)*255));
				rect(x, y,10,10);	
			}
		}
		
		
	
		
		fill(255,0,0);
		textSize(10);
		text("Nombre entrainement: "+session,10,10);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
